SHELL=/bin/sh
CFLAGS=-std=gnu99 -pedantic -Wall -g -O0 -fstack-protector
LDFLAGS=-rdynamic
DEST=epoch

src = $(wildcard src/*.c)
obj = $(src:.c=.o)


epoch: $(obj) 
	$(CC) -o $@ $^ $(CFLAGS) $(LDFLAGS)

.PHONY: clean 

clean:
	rm -f $(DEST)
	rm -f $(obj) 

install: epoch
	mkdir -p $(DESTDIR)/sbin
	cp -f epoch $(DESTDIR)/sbin/
	mkdir -p $(DESTDIR)/etc/epoch
	cp -f etc/epoch/epoch.conf $(DESTDIR)/etc/epoch/
	mkdir -p $(DESTDIR)/usr/share/doc/epoch
	cp -f usr/share/doc/epoch/README $(DESTDIR)/usr/share/doc/epoch/
	cp -f usr/share/doc/epoch/UNLICENSE.TXT $(DESTDIR)/usr/share/doc/epoch/
	mkdir -p $(DESTDIR)/usr/share/man/man1/
	cp -f usr/share/man/man1/epoch.1 $(DESTDIR)/usr/share/man/man1/
uninstall:
	rm -f $(DESTDIR)/sbin/epoch
	rm -f $(DESTDIR)/etc/epoch/epoch.conf
	rm -f $(DESTDIR)/usr/share/doc/epoch/README
	rm -f $(DESTDIR)/usr/share/doc/epoch/UNLICENSE.TXT
	rm -f $(DESTDIR)/usr/share/man/man1/epoch.1 
	rmdir -p $(DESTDIR)/usr/share/doc/epoch/
	rmdir -p $(DESTDIR)/etc/epoch/
	rmdir -p $(DESTDIR)/usr/share/man/man1/
